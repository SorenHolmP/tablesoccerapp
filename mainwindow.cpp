#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QString>
#include <QDebug>

#include <iostream>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0); //Set initial page to page 0

}

MainWindow::~MainWindow()
{
    delete ui;
}

/*** Slots for button presses ***/
void MainWindow::on_reportMatchButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2); //Go to report match page
    emit get_avail_players_emitter();

    for(auto p : _currently_avail_players)
    {
        QString rating = QString::fromStdString( std::to_string(p.getRating()) );
        ui->comboTeam1left->addItem(p.getName() + " (" + rating + ")");
        ui->comboTeam1right->addItem(p.getName() + " (" + rating + ")");
        ui->comboTeam2left->addItem(p.getName() + " (" + rating + ")");
        ui->comboTeam2right->addItem(p.getName() + " (" + rating + ")");

    }

}

void MainWindow::on_backToMenuButton_clicked()
{

    ui->stackedWidget->setCurrentIndex(0); //Go to menu page

    //Clear the comboboxes:
    ui->comboTeam1left->clear();
    ui->comboTeam1right->clear();
    ui->comboTeam2left->clear();
    ui->comboTeam2right->clear();
}

void MainWindow::on_backToMenuButton_2_clicked() //Go back to menu from add player page
{

    ui->stackedWidget->setCurrentIndex(MENU_PAGE);
    ui->lineEdit_name->clear();     //Clear the line edit fields:
    ui->lineEdit_rating->clear();
}

void MainWindow::on_addPlayerButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(ADD_PLAYER_PAGE);
}
void MainWindow::on_comboTeam1left_activated(int index)
{
}

void MainWindow::get_avail_players_emitter()
{
    emit signal_get_avail_players();
}

void MainWindow::on_available_players_posted(std::vector<Player> &avail_players)
{
    _currently_avail_players = avail_players;
    qDebug() << "Mainwindow received the following players:\n";
    for(auto p : avail_players)
    {
        //std::cout << "player received: " << avail_players[i] << std::endl;
        //ui->comboTeam1right->addItem(avail_players[i]);
        //Debug() << "LOL";
        qDebug() << p.getName();
    }
}



/**** SIGNALS ****/

void MainWindow::on_reportResultButton_clicked()
{
    //Set up teams:
    Player p1 = _currently_avail_players[ ui->comboTeam1left->currentIndex() ];
    Player p2 = _currently_avail_players[ ui->comboTeam1right->currentIndex() ];
    Player p3 = _currently_avail_players[ ui->comboTeam2left->currentIndex() ];
    Player p4 = _currently_avail_players[ ui->comboTeam2right->currentIndex() ];

    std::vector<Player> team1 = {p1,p2}, team2 = {p3,p4};

    //Get result:
    qDebug() << "Current string in team1 line edit: " << ui->lineEditTeam1->displayText();

    int result_team1 = ui->lineEditTeam1->displayText().toInt();
    int result_team2 = ui->lineEditTeam2->displayText().toInt();
    std::vector<int> result = {result_team1, result_team2};

    QString date = _date.currentDateTime().toString();

    Match m(team1, team2, result, date);

    emit report_match_signal(m); //send the match info to database
}



void MainWindow::on_AddPlayerToDatabaseButton_clicked()
{
    //Get data from line edits:
    QString playerName = ui->lineEdit_name->displayText();
    int playerRating = ui->lineEdit_rating->displayText().toInt();

    if(playerRating == 0) //If the player rating field is not specified -- then use default value
        playerRating = 1200;
    if(playerName != "") //What if the playerName is already in database?
    {
        qDebug() << "Added " << playerName << " to database";
        Player p(playerName, playerRating);
        emit signal_add_player_to_database(p);
        return;
    }

    qDebug() << "Error did not add player -- no player name specified";

}
