#include "match.h"

Match::Match()
{

}

Match::Match(std::vector<Player> team1, std::vector<Player> team2, std::vector<int> result, QString date)
    : _team1(team1), _team2(team2), _result(result), _date(date)
{

}

std::vector<int> Match::getResult()
{
    return _result;
}

std::vector<Player> Match::getTeam1()
{
    return _team1;
}

std::vector<Player> Match::getTeam2()
{
    return _team2;
}
