#include "mainwindow.h"
#include <QApplication>
#include <QObject>

#include "database.h"
#include "player.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //Create database and a few players:
    Database db;
    db.addPlayer("Søren");
    db.addPlayer("Niko");
    db.addPlayer("Whalen");
    db.addPlayer("Jenni");

    db.displayPlayers();

    MainWindow w;

    QObject::connect(&w,&MainWindow::signal_get_avail_players, &db, &Database::retrieve_available_players);
    QObject::connect(&db, &Database::post_available_players, &w, &MainWindow::on_available_players_posted);

    QObject::connect(&w, &MainWindow::report_match_signal, &db, &Database::add_match_slot);
    QObject::connect(&w, &MainWindow::signal_add_player_to_database, &db, &Database::add_player_slot);


    w.show();

    return a.exec();
}
