#ifndef MATCH_H
#define MATCH_H


#include <QString>

#include <vector>

#include "player.h"


class Match
{
public:
    Match();
    Match(std::vector<Player> _team1, std::vector<Player> _team2, std::vector<int> _result, QString date);
    std::vector<int> getResult();
    std::vector<Player> getTeam1();
    std::vector<Player> getTeam2();

private:
    std::vector<Player> _team1;
    std::vector<Player> _team2;
    std::vector<int> _result; //[team1, team2]
    QString _date;

};

#endif // MATCH_H
