#include "database.h"

using namespace std;

Database::Database(QObject *parent) : QObject(parent)
{

}

void Database::displayPlayers()
{
    qDebug() << "Listing all players and their stats: \n";

    qDebug() << "---------------------------------------\n";

    for(auto p : _playerList)
    {
        qDebug() << p.getName() << " (" << p.getId() << ")" << "\n";
        qDebug() << "ELO: " << p.getRating();
        qDebug() << "\n---------------------------------------\n";
    }
}


void Database::addPlayer(Player p)
{
    _playerList.push_back(p);
}

void Database::addPlayer(QString name)
{
    Player dummy(name, 1200, _idCounter);
    _playerList.push_back(dummy);
    _idCounter++;
}


void Database::removePlayer(Player p)
{
    //Find index of player p in playerList:

    for(int i = 0; i < _playerList.size(); i++)
    {
        if(p.getId() == _playerList[i].getId())
        {
            _playerList.erase(_playerList.begin() + i);
            qDebug() << "removed " << p.getName() << " from database" << "\n";
            break;
        }
    }
}

int Database::isWin(int x) // returns 1 for win, 0 for lose
{
    if (x == 10)
    {
        return 1;
    }
    return 0;
}

/**** SLOTS ****/

void Database::retrieve_available_players()
{
    emit post_available_players(_playerList);
}

void Database::add_match_slot(Match m)
{
    _matchHistory.push_back(m);
    std::vector<Player> players;
    players.push_back(m.getTeam1().at(0));
    players.push_back(m.getTeam1().at(1));
    players.push_back(m.getTeam2().at(0));
    players.push_back(m.getTeam2().at(1));

    //Init variables for rating calculatings
    double eloAvgTeam1 = players.at(0).getRating() + players.at(1).getRating();
    double eloAvgTeam2 = players.at(2).getRating() + players.at(3).getRating();
    double difference = eloAvgTeam1 - eloAvgTeam2;
    int n = 400; // Represents a “10X” skill differential
    int k = 70; // Sensitivity factor

    //Calculate expected % for win
    double exponent = -(difference/n);
    double x = qPow(1,exponent);
    double E = 1/(1+x); //Expected win %

    int diff;
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < _playerList.size(); j++){
             if(players.at(i).getId() == _playerList.at(j).getId()){
                 if(i < 2){
                    diff = k * (isWin(m.getResult().at(0)) - E); //Uses result for player 1 and 2
                 } else {
                    diff = k * (isWin(m.getResult().at(1)) - E); //Uses result for player 3 and 4
                 }
                _playerList.at(i).updateRating(diff);
             }
        }
    }

    /*
     * Implement logic to update rating, winrates, win-losses etc.
     */
    qDebug() << "Number of matches played: " << _matchHistory.size();
}

void Database::add_player_slot(Player p)
{
    _playerList.push_back(p);
    qDebug() << "Just added " << p.getName() << " to database";
}



