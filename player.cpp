#include "player.h"

using namespace std;


Player::Player(QString name, int rating): _name(name), _rating(rating)
{

}

Player::Player(QString name, int rating, int id): _name(name), _rating(rating), _id(id)
{

}


QString Player::getName()
{
    return _name;
}


int Player::getRating()
{
    return _rating;
}

int Player::getId()
{
    return _id;
}

void Player::updateRating(double diff)
{
    _rating += diff;
}
