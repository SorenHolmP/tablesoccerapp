#ifndef DATABASE_H
#define DATABASE_H


//Qt includes:
#include <QObject>
#include <QDebug>
#include <QtMath>

//c++ std lib:
#include <vector>
#include <iostream>

//
#include "player.h"
#include "match.h"

class Database : public QObject
{
    Q_OBJECT
public:
    explicit Database(QObject *parent = nullptr);
    void displayPlayers();
    void addPlayer(QString name);
    void addPlayer(Player p);
    void removePlayer(Player p);
    int isWin(int x);

signals:
    void post_available_players(std::vector<Player> &players);
public slots:
    void retrieve_available_players();
    void add_match_slot(Match m);
    void add_player_slot(Player p);

private:
    std::vector<Player> _playerList;
    std::vector<Match>  _matchHistory;
    int _idCounter = 0;


};

#endif // DATABASE_H
