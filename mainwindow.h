#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//Qt:
#include <QMainWindow>
#include <QDateTime>

//c++ std lib:
#include <vector>

//
#include "player.h"
#include "match.h"
#include "defines.h" //Contains defines for page numbers etc.

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void signalEmitter();
    void get_avail_players_emitter();



signals:
    void signal_get_avail_players();
    void signal_add_player_to_database(Player p);
    void report_match_signal(Match m);

public slots:
    void on_reportMatchButton_clicked();

    void on_comboTeam1left_activated(int index);

    void on_available_players_posted(std::vector<Player> &avail_players);



private slots:
    void on_backToMenuButton_clicked();

    void on_reportResultButton_clicked();

    void on_backToMenuButton_2_clicked();

    void on_addPlayerButton_clicked();

    void on_AddPlayerToDatabaseButton_clicked();

private:
    Ui::MainWindow *ui;
    std::vector<Player> _currently_avail_players;
    QDateTime _date;
};

#endif // MAINWINDOW_H
