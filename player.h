#ifndef PLAYER_H
#define PLAYER_H

#include <QString>
#include <iostream>

class Player
{
public:
    //explicit Player(QObject *parent = nullptr);
    Player(QString name);
    Player(QString name, int rating);
    Player(QString name, int rating, int id);

    QString getName();
    int getRating();
    int getId();
    void updateRating(double diff);

private:
    QString     _name;
    int         _rating;
    int         _id;

};

#endif // PLAYER_H
