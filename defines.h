#ifndef DEFINES_H
#define DEFINES_H

#define MENU_PAGE           0
#define ADD_PLAYER_PAGE     1
#define REPORT_RESULT_PAGE  2
#define STATS_PAGE          3

#endif // DEFINES_H
